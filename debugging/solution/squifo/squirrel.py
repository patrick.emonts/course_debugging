from enum import Enum

class SquirrelType(Enum):
    EUROPEAN = 1
    AMERICAN = 2

class Squirrel:
    def __init__(self, squirrel_type: SquirrelType):
        self.type = squirrel_type
        self.fluffy = False

    def is_fluffy(self) -> bool:
        return self.fluffy

    def set_fluffy(self):
        self.fluffy = True

    def get_squirreltype(self) -> SquirrelType:
        return self.type