from squifo.tree import Pine, Apple
from squifo.forest import Forest
from squifo.squirrel import SquirrelType

def main():
    pine = Pine(15.0, 1.0)
    pine.add_squirrel(SquirrelType.AMERICAN)
    pine.add_squirrel(SquirrelType.EUROPEAN)
    pine.add_squirrel(SquirrelType.EUROPEAN)

    apple = Apple(10.0, 0.3, 10)

    forest = Forest()
    forest.add_tree(pine)
    forest.add_tree(apple)

    print("Number of Conifers:", forest.calculate_number_conifer())
    print("Number of Squirrels:", forest.calculate_number_squirrel())

    forest.exterminate_american_squirrels()
    print("Killed all American Squirrels")
    
    print("Number of Squirrels:", forest.calculate_number_squirrel())

if __name__ == "__main__":
    main()