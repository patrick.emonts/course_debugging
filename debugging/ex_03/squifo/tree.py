from enum import Enum
from typing import List
from abc import ABC
from squifo.squirrel import SquirrelType

class Squirrel:
    def __init__(self, squirrel_type: SquirrelType):
        self.squirrel_type = squirrel_type

class Tree(ABC):
    def __init__(self, height: float, radius: float):
        self._height = height
        self._radius = radius
        self.name = ""
        self.squirrellist: List[Squirrel] = []

    @property
    def height(self) -> float:
        return self._height

    @height.setter
    def height(self, height: float):
        self._height = height

    @property
    def radius(self) -> float:
        return self._radius

    @radius.setter
    def radius(self, radius: float):
        self._radius = radius

    def is_conifer(self) -> bool:
        raise NotImplementedError("Subclasses must implement this method")

    def add_squirrel(self, squirrel_type: SquirrelType):
        """Add a squirrel to the tree.

        Args:
            squirrel_type (SquirrelType): Squirrel to be added
        """
        self.squirrellist.append(Squirrel(squirrel_type))

    def get_squirrellist(self) -> List[Squirrel]:
        return self.squirrellist

    def kill_squirrel_type(self, squirrel_type: SquirrelType):
        """Kill all squirrels of a certain type.

        Args:
            squirrel_type (SquirrelType): Squirrel Type to be deleted.
        """
        self.squirrellist = [squirrel for squirrel in self.squirrellist if squirrel.squirrel_type != squirrel_type]

class Fir(Tree):
    def __init__(self, height: float, radius: float):
        super().__init__(height, radius)

    def is_conifer(self) -> bool:
        return True

class Pine(Tree):
    def __init__(self, height: float, radius: float):
        super().__init__(height, radius)

    def is_conifer(self) -> bool:
        return True

class Apple(Tree):
    def __init__(self, height: float, radius: float, n_apples: int):
        super().__init__(height, radius)
        self.n_apples = n_apples
        self.ripe = False

    def is_conifer(self) -> bool:
        return False

    def ripen(self):
        """Set the appels to ripe."""
        self.ripe = True

    def is_ripe(self) -> bool:
        return self.ripe