import numpy as np

def swap(arr, i,j):
    """Swap the entries at two locations in an array.

    Args:
        arr (np.ndarray): Input array
        i (int): Index of the first entry
        j (int): Index of the second entry
    """
    arr[i],arr[j] = arr[j],arr[i]

def bubble_sort(arr):
    """Apply the bubble sort array to a copy of arr. The original array stays unchanged.

    Args:
        arr (np.ndarray): Input array

    Returns:
        np.ndarray: Sorted array
    """
    dest = np.copy(arr)
    n = len(dest)
    for i in range(n - 1):
        for j in range(0, n - i):
            if dest[j] > dest[j + 1]:
                swap(dest,i,j+1)
    return dest

def main():
    n = 10
    array = [np.random.randint(1, 10) for _ in range(n)]

    print("Unsorted list:")
    for num in array:
        print(num)

    dest = bubble_sort(array)

    print("Sorted list in ascending order:")
    for num in dest:
        print(num)

if __name__ == "__main__":
    main()