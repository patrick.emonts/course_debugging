import unittest
import numpy as np
from bubblesort import bubble_sort, swap

class TestBubbleSort(unittest.TestCase):
    def setUp(self) -> None:
        return super().setUp()
    
    def test_rnd_array(self):
        N=10
        for i in range(100):
            arr = np.random.randint(10,size=N)
            val = bubble_sort(arr)
            ref = np.asarray(sorted(arr))
            self.assertTrue(np.allclose(val,ref))
    
    def test_swap(self):
        val = np.array([3, 1])
        swap(val, 0, 1)
        ref = np.array([1, 3])
        self.assertTrue(np.allclose(val, ref))

if __name__ == "__main__":
    unittest.main()