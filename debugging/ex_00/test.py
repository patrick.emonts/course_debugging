import unittest
import numpy as np
from bubblesort import bubble_sort

class TestBubbleSort(unittest.TestCase):
    def setUp(self) -> None:
        return super().setUp()
    
    def test_rnd_array(self):
        N=10
        for i in range(100):
            arr = np.random.randint(10,size=N)
            val = bubble_sort(arr)
            ref = np.asarray(sorted(arr))
            self.assertTrue(np.allclose(val,ref))
    
    def test_swap(self):
        pass

if __name__ == "__main__":
    unittest.main()