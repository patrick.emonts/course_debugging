from typing import List
from squifo.tree import Tree
from squifo.squirrel import SquirrelType

class Forest:
    def __init__(self):
        self.treevec: List[Tree] = []

    def add_tree(self, tree: Tree):
        """Add a tree to the forest.

        Args:
            tree (Tree): Tree to be added
        """
        self.treevec.append(tree)

    def calculate_number_conifer(self) -> int:
        """Count the number of conifers present in the forest

        Returns:
            int: Number of conifers
        """
        return sum(1 for tree in self.treevec if tree.is_conifer())

    def exterminate_american_squirrels(self):
        """Delete all american squirrels from the forrest.
        """
        for tree in self.treevec:
            if tree:
                tree.kill_squirrel_type(SquirrelType.AMERICAN)

    def calculate_number_squirrel(self) -> int:
        """Count the number of squirrels in the whole forest.

        Returns:
            int: Number of squirrels
        """
        return sum(len(tree.get_squirrellist()) for tree in self.treevec if tree)