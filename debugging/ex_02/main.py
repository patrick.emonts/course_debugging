from squifo.tree import Pine, Apple, Tree
from squifo.forest import Forest
from squifo.squirrel import SquirrelType


def generate_tree(name: str) -> Tree:
    """Generate an example tree given the tree's name.

    Args:
        name (str): Name of the tree

    Returns:
        Tree: An example tree
    """
    dest = None
    if name=="apple":
        dest = Apple(10.0, 0.3, 10)
    elif name =="pine":
        dest = Pine(15.0, 1.0)
        dest.add_squirrel(SquirrelType.AMERICAN)
        dest.add_squirrel(SquirrelType.EUROPEAN)
        dest.add_squirrel(SquirrelType.EUROPEAN)
        dest.add_squirrel(SquirrelType.EUROPAEN)
    return dest

def main():
    forest = Forest()
    pine = generate_tree("pine")
    forest.add_tree(pine)
    apple = generate_tree("apple")
    forest.add_tree(apple)
    birch = generate_tree("birch")
    forest.add_tree(birch)

    print("Number of Conifers:", forest.calculate_number_conifer())
    print("Number of Squirrels:", forest.calculate_number_squirrel())

    forest.exterminate_american_squirrels()
    print("Killed all American Squirrels")
    
    print("Number of Squirrels:", forest.calculate_number_squirrel())

if __name__ == "__main__":
    main()