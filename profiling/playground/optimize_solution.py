
import numpy as np

def some_matrix_function(mat_a: np.ndarray, mat_b: np.ndarray,
                           mat_c: np.ndarray,
                           mat_d: np.ndarray) -> float:
    dest = -0.5 * np.trace(mat_a @ mat_c @ mat_d @ mat_b)
    return dest

def some_matrix_function_opt(mat_a: np.ndarray, mat_b: np.ndarray,
                           mat_c: np.ndarray,
                           mat_d: np.ndarray) -> float:
    A = mat_a@mat_c
    B = mat_d @ mat_b
    dest = -0.5 * (A*B.T).sum()
    return dest
