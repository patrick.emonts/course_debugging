import numpy as np

def some_matrix_function(mat_a: np.ndarray, mat_b: np.ndarray,
                           mat_c: np.ndarray,
                           mat_d: np.ndarray) -> float:
    dest = -0.5 * np.trace(mat_a @ mat_c @ mat_d @ mat_b)
    return dest
