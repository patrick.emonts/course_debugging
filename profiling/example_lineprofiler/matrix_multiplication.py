import numpy as np
from numba import njit, prange

# Loading the line profiler and making it print upon termination of the program
import line_profiler
import atexit
profile = line_profiler.LineProfiler()
atexit.register(profile.print_stats)

@njit(parallel = True)
def matrix_multiplication_numba_parallel(mat_a, mat_b):
    m_a, n_a = mat_a.shape
    m_b, n_b = mat_b.shape
    dest = np.zeros((m_a,n_b))
    for i in prange(m_a):
        for j in range(n_b):
            for k in range(n_a):
                dest[i,j]+=mat_a[i,k]*mat_b[k,j]
    return dest

@njit
def matrix_multiplication_numba(mat_a, mat_b):
    m_a, n_a = mat_a.shape
    m_b, n_b = mat_b.shape
    dest = np.zeros((m_a,n_b))
    for i in range(m_a):
        for j in range(n_b):
            for k in range(n_a):
                dest[i,j]+=mat_a[i,k]*mat_b[k,j]
    return dest

# @profile
def matrix_multiplication_python(mat_a, mat_b):
    m_a, n_a = mat_a.shape
    m_b, n_b = mat_b.shape
    dest = np.zeros((m_a,n_b))
    for i in range(m_a):
        for j in range(n_b):
            for k in range(n_a):
                dest[i,j]+=mat_a[i,k]*mat_b[k,j]
    return dest

# @profile
def matrix_multiplication_numpy(mat_a, mat_b):
    return mat_a@mat_b

# This makes line_profiler profile a function
@profile
def main(N):
    rnd_mat_a = np.random.rand(N, N)
    rnd_mat_b = np.random.rand(N, N)

    mat_c_python = matrix_multiplication_python(rnd_mat_a, rnd_mat_b)
    mat_c_np = matrix_multiplication_numpy(rnd_mat_a, rnd_mat_b)
    mat_c_numba = matrix_multiplication_numba(rnd_mat_a, rnd_mat_b)
    mat_c_numba_parallel = matrix_multiplication_numba_parallel(rnd_mat_a, rnd_mat_b)

    are_equal = True
    are_equal &= np.allclose(mat_c_python, mat_c_np)
    are_equal &= np.allclose(mat_c_python, mat_c_numba)
    are_equal &= np.allclose(mat_c_python, mat_c_numba_parallel)
    print(f"All matrices are identical: {are_equal}")

if __name__=="__main__":
    import argparse
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("N", type=int, help = "Size of the matrix")
    args = parser.parse_args()
    main(args.N)