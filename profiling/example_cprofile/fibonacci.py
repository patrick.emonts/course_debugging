import numpy as np

# For more information on cProfile and visualization: 
# https://www.machinelearningplus.com/python/cprofile-how-to-profile-your-python-code/?utm_content=cmp-true

def fibonacci_recursive(n):
    """
    Generate the first N Fibonacci numbers using recursion.

    Parameters:
    - n (int): The number of Fibonacci numbers to generate.

    Returns:
    - list: A list containing the first N Fibonacci numbers.
    """
    if n <= 0:
        return []
    elif n == 1:
        return [0]
    elif n == 2:
        return [0, 1]
    else:
        fib_sequence = fibonacci_recursive(n - 1)
        fib_sequence.append(fib_sequence[-1] + fib_sequence[-2])
        return fib_sequence

def fibonacci_iterative(n):
    """
    Generate the first N Fibonacci numbers using iteration.

    Parameters:
    - n (int): The number of Fibonacci numbers to generate.

    Returns:
    - list: A list containing the first N Fibonacci numbers.
    """
    fib_sequence = [0, 1]
    for i in range(2, n):
        fib_sequence.append(fib_sequence[-1] + fib_sequence[-2])
    return fib_sequence[:n]

if __name__=="__main__":
    N = 20
    fib_it = np.asarray(fibonacci_iterative(N))
    fib_rec = np.asarray(fibonacci_recursive(N))
    print(f"The two Fibonacchi sequences are identical: {np.allclose(fib_it,fib_rec)}")