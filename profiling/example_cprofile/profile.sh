#!/bin/bash
set -eu
python -m cProfile -o prof.out fibonacci.py
snakeviz prof.out