# Debugging and Profiling Exercises

This repository contains code with intentional bugs and example code for profiling.

## Debugging
This folder contains code with intentional bugs to train debugging with an IDE (Integrated Development Environment).

- `ex_01`: Bug in a bubblesort implementation
- `ex_02`: Bug in a bubblesort implementation
- `ex_03`: Bug in a object-oriented Python code
- `ex_04`: Bug in a object-oriented Python code

## Profiling

- `example_lineprofiler`: An example of a running profiler with `line_profiler`
- `example_cprofile`: An example of a running profiler with `cProfile`
- `playground`: Some functions that you can profile and optimize

